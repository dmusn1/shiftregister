# Arduino explain register

## 
Este es un sketch que servirá para explicar el funcionamiento de los registros de desplazamiento.

###

**reloj -- clockPin**
El reloj es un bit que permitirá recibir datos, por tanto debe crear un ciclo de 8 flancos en alto para poder diferenciar 8 registros.

```
delay(time0);
digitalWrite(clockPin,HIGH);  
delay(time0);
digitalWrite(clockPin,LOW);   
```
**Datos -- dataPin**
Datos envía flancos en alto en cada bit en donde queremos que el registro agregue un valor en 1 en el ejemplo manda datos cuando el contador del bucle es 0 y 5.

```
if(i==0 or i==5){ 
        digitalWrite(dataPin,HIGH);
        delay(time1);                 
        digitalWrite(clockPin,HIGH); 
        delay(time1);
        digitalWrite(dataPin,LOW);  
        delay(time1);
        digitalWrite(clockPin,LOW);   
        delay(time1);
        
}        
```

**Latch -- latchPin**
Envía un flanco en alto posterior a la lectura de los registros y habilita la salida.

```
digitalWrite(latchPin, LOW);
 //Codigo de registros
digitalWrite(latchPin, HIGH);
```



![alt text](https://programarfacil.com/wp-content/uploads/2016/02/Cronograma74HC595.jpg) .




fuente: https://programarfacil.com/blog/74hc595-registro-de-desplazamiento-arduino/ .

Simulación en thinkercad: https://www.tinkercad.com/things/hQ2n2rlu5av-spectacular-snaget/editel?sharecode=sP9NG9lFXTSCuoyeRh-gHsHYwhCfEKszL60xF7Fgzz8
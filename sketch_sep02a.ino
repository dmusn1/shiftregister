/*
UNIVERSIDAD MARIANO GALVEZ DE GUATEMALA
SEDE COBÁN 
PLAN FIN DE SEMANA
ELECTRÓNICA DIGITAL
Diego Mus Noack

*/

int latchPin = 5;            //  pin 12 74595
int clockPin = 4;           // pin 11  74595
int dataPin = 2;            // pin 14 74595
int time1 = 100;              // variable de tiempo cuando data envía datos
int time0 = 200;              // Variable de tiempo de reloj  

//----------------------Declaración de Variables__________________

void setup() {
  pinMode(latchPin, OUTPUT); 
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT); 
}


//----------------------bucle-------------------
void loop() {
  digitalWrite(latchPin, LOW);  //Latch en bajo como iniciación del registro de datos
    //CICLO DE 8 FLANCOS DEL RELOJ       
    for(int i=0; i<8; i++){ //Ciclo de lectura de 8 bits
        if(i==0 or i==5){ // Condicion en que registro se enviarán flacos data 
        digitalWrite(dataPin,HIGH);  //Entrada de data con flanco en alto
        delay(time1);                 
        digitalWrite(clockPin,HIGH); //Reloj en alto para dejar ingresar valores de data 
        delay(time1);
        digitalWrite(dataPin,LOW);   // Cierre de envio de data
        delay(time1);
        digitalWrite(clockPin,LOW);   // Cierre de lectura de Reloj
        delay(time1);
        }else{
        delay(time0);
        digitalWrite(clockPin,HIGH);  // Flanco en alto de reloj
        delay(time0);
        digitalWrite(clockPin,LOW);   //Flanco en bajo de reloj
        }
    }
  digitalWrite(latchPin, HIGH);  //flanco en alto de latch para aceptar los valores de registro
  

  
 //--------------Bloque de codigo para borrar información ------------------------- 
 delay(1000);
 digitalWrite(latchPin,LOW);
 shiftOut(dataPin, clockPin, MSBFIRST,0); 
 digitalWrite(latchPin,HIGH);
 delay(1000);

 //-------------------------------------------------------------------------------
}